# Marketing Frontend Developer - Challenge

#### 1. My recent favourite Javascript framework is React. Even though I am still getting acquainted with it, I feel it lets you create predictable and declarative user interfaces while the treating the code as black box. Angular.js is another similar framework whose intent is similar to React and it was released before React. But in my opinion, React has better error handling as compared to Angular. React detects error in design time or compile time whereas Angular throws errors on runtime. But I am still too inexperienced with either of the two technologies to make a conclusive comparison. Both of them might have advantages and disadvantages depending upon the project.
#### 2. [https://repl.it/HTzn/0](https://repl.it/HTzn/0)
```
function fizzBuzz(n) {
  for (let i = 1; i <= n; i++) {
    let f = i % 3 === 0;
    let b = i % 5 === 0;

    console.log(f ? b ? "FizzBuzz" : "Fizz" : b ? "Buzz" : i);

    // Expansion of above condition
    // if (i % 3 === 0) {
    //   if (i % 5 === 0) {
    //     console.log("FizzBuzz");
    //   } else {
    //     console.log("Fizz");
    //   }
    // } else if (i % 5 === 0) {
    //   console.log("Buzz")
    // } else {
    //   console.log(i);
    // }

  }
}
```
#### 3. correct way to create a javascript array
```
var items = ["Orange","Apple"]; // correct answer, this is the preferred way
var items = {"Orange","Apple"}; // incorrect syntax
var items = new array("Orange","Apple"); // the name of the function "Array" should be capitalised
// the above is correct but the function can be overridden creating inconsistencies
var items[] = {"Orange","Apple"}; // incorrect syntax
```
#### 4. adding an element value to the end of a JavaScript array
```
arr[arr.length+1] = value // skips an element and adds an element one place after the last one
arr[arr.length] = value // correct, adds an element after the last one, returns the value of the element added
arr[arr.length-1] = value // replaces the last element with value
arr = arr + value // arr does not remain an array anymore
arr.push(value) // correct, adds an element after the last one, returns the new length of the array
arr.append(value) // incorrect, it is a jQuery function
```
#### 5. "== compares value. === compares value and type" is the most accurate answer.
#### 6. The status code 500 denotes a server error.
#### 7. [https://repl.it/HTy4/0](https://repl.it/HTy4/0)
```
function firstRepeatingLetter(s)
{
  return s.match(/(.)(?=(.*\1)+?)/g)[0];
}
```
